package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
   
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    List<String> gameChoices = Arrays.asList("y", "n");
	String continueAnswer = "y";
   

    String rspChoicesSingle = ("rock paper scissors");
    
    public String RandomChoice() {
    	int rspSize = rpsChoices.size();
        int rnd = new Random().nextInt(rspSize);
        if (rnd==0) {
        	return "rock";
        }
        else if (rnd==1) {
        	return "paper";
        }
        else {
        	return "scissors";
        }
        	
    }
    
    
    
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }


	public boolean IsWinner(String choice1, String choice2) {
    /*return true if choice1 beats choice2, false if not*/
		if (choice1.equals("paper")) {
        	if (choice2.equals("rock")) {
        		return true;}
        	else {
        		return false;
        	}
        }
        
        else if (choice1.equals("scissors")) {
        	if (choice2.equals("paper")) {
        		return true;}
        	else {
        		return false;
        	}
        }
        
        else if (choice1.equals("rock")) {
        	if (choice2.equals("scissors")) {
        		return true;}
        	else {
        		return false;
        	}
        }
        else {
        	return false;
        }
	}
    
    
    public String userChoice() {
        /*Prompt the user with what choice of rock paper scissors they choose
        *:return: "rock", "paper" or "scissors"
        */
    	String inputText = null;
    	String resultText = null;
    	boolean validInput = false;
        while (validInput == false) {
        	
            

            inputText = readInput("Your choice (Rock/Paper/Scissors)?");  // Read user input
            
            String input = inputText;

 
            List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
            
            

            	
            boolean result = rpsChoices.stream().anyMatch(s -> input.matches(".*\\b" + s + "\\b.*"));
            if (result) {
            	
            	validInput = true;
            	
            	resultText = input;
            
            }
            else {
            	
        		System.out.print ("I don't understand ");
				
        		System.out.print (inputText);
        		
        		System.out.println (" Try again");
            }
            	
        }
        return resultText;
		
    }
		

		
    
    
	    public String validateInput() {
	        /*test if y or n is used	        */
	    	String continueAnswer = null;
	    	boolean validInput2 = false;
	        while (validInput2 == false) {
	        	
	            

				continueAnswer = readInput("Do you wish to continue playing? (y/n)?"); // Read user input
				
				String input2 = continueAnswer;
	            
	            boolean choiceTest2 = gameChoices.stream().anyMatch(s -> input2.matches(".*\\b" + s + "\\b.*"));
	            if (choiceTest2) {
				

            		validInput2 = true;
            		
	            		
	            }
	            
	            else {
	            	
	            		System.out.print ("I do not understand ");
					
	            		System.out.print (continueAnswer);
	            		System.out.println (". Could you try again");
	            	
	            	}
	        }

	        return continueAnswer;
	    }
	               	
            



	

	
	
		
/*
Checks if the given input is either rock, paper or scissors.
:param input: user input string
:param valid_input: list of valid input strings
:return: true if valid input, false if not

					input <= input.lower()
					return input in valid_input

*/
	    public void run() {
/*Game loop*/

	    	while (continueAnswer =="y") {
	    		
				System.out.println ("Let's play round " + roundCounter);
/*Human and Computer choice*/
				
				String HC = userChoice();
				
				String computerChoice = RandomChoice();
					String choice_string = ("Human chose " + HC + ", computer chose " + computerChoice + ".");

/*Check who won*/
					boolean userWin = IsWinner(HC, computerChoice);
					boolean computerWin = IsWinner(computerChoice, HC);
				
						if  (userWin) {
							System.out.println(choice_string + " Human wins!");
							humanScore = humanScore+1;
							System.out.println("Score: human " + humanScore + ", computer " + computerScore);
						}
				
						else if (computerWin) {
							System.out.println(choice_string + " Computer wins!");
							computerScore = computerScore+1;
							System.out.println("Score: human " + humanScore + ", computer " + computerScore);
						}
				
						else {
							System.out.println(choice_string + " It's a tie!");
							System.out.println("Score: human " + humanScore + ", computer " + computerScore);
						}
						roundCounter = roundCounter + 1;
				
/* Ask if human wants to play again*/
				String ending = validateInput();
			
				
						if (ending.equals("n")) {
							System.out.print("Bye bye :)");
							continueAnswer = "n";
						}
					}
				
			}
	    
}

